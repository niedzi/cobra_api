class Mutations::CreateUser < GraphQL::Function
  # AuthProviderInput = GraphQL::InputObjectType.define do
  #   name 'AuthProviderSignupData'
  #
  #   argument :email, Types::AuthProviderEmailInput
  # end

  argument :name, !types.String
  argument :authProvider, !Types::AuthProviderEmailInput

  type Types::UserType

  def call(obj, args, ctx)
    User.create!(
        name: args[:name],
        email: args[:authProvider][:email],
        password: args[:authProvider][:password]
    )
  # rescue ActiveRecord::RecordInvalid => e
  #   GraphQL::ExecutionError.new("Invalid input: #{e.record.errors.full_messages.join(', ')}")
  end
end