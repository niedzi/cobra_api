Types::QueryType = GraphQL::ObjectType.define do
  name "Query"
  # Add root-level fields here.
  # They will be entry points for queries on your schema.

  field :users, !types[Types::UserType] do
    # types[Types::UserType]

    # argument :email, types.String

    description "User"
    resolve ->(obj, args, ctx) {
      User.all
    }
  end
end
